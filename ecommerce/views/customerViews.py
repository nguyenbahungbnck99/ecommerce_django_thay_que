from datetime import datetime
from ecommerce.views.views import login
from django.http.response import HttpResponseNotAllowed, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.contrib import messages
from ..models import Address, Bank, Cart, Deliveryaddress, Inventory, Invoice, Name, Order, Person, Product, Province, Transportation, User
from ..currentAuthen import authen


def homepage(request):
    if request.method == "GET":
        # print(authen.username)
        # if not authen.checkAuthen():
        #     return redirect("login")

        product = Product.objects.all()
        for item in product:
            price = item.price
            sale = item.sale
            salePrice = price * (100 - sale) / 100
            item.salePrice = round(salePrice)

        res = {"product": product}

        return render(request, "customer/homepage.html", res)


def register(request):
    if request.method == "GET":
        return render(request, "customer/register.html")
    else:
        username = request.POST.get("username")
        password = request.POST.get("password")
        repassword = request.POST.get("repassword")
        first_name = request.POST.get("first_name")
        last_name = request.POST.get("last_name")
        gender = request.POST.get("gender")
        phone = request.POST.get("Phone")
        email = request.POST.get("Email")
        birth = request.POST.get("birth")
        # address = request.POST.get("address")

        if (
            len(username) <= 0
            or len(password) <= 0
            or len(repassword) <= 0
            or len(first_name) <= 0
            or len(last_name) <= 0
            or len(gender) <= 0
            or len(phone) <= 0
            or len(email) <= 0
            or len(birth) <= 0
            # or len(address) <= 0
        ):
            messages.info(request, "Bạn chưa nhập đủ thông tin")
            return redirect("register")

        print(username, password, repassword)

        existEmail = User.objects.filter(username=username).exists()
        if existEmail:
            messages.info(request, "Đã tồn tại tài khoản này")
            return redirect("register")

        if password != repassword:
            messages.info(request, "Không trùng mật khẩu")
            return redirect("register")

        user = User(username=username, pass_field=password, role="Customer")
        user.save()

        # get lastest id
        userId = User.objects.latest("id")
        person = Person(
            userid=userId, gender=gender, birthday=birth, phone=phone, email=email
        )
        person.save()
        name = Name(userid=userId,first_name=first_name,last_name=last_name)

        name.save()

        # get lastest person id
        # lastestPerson = Person.objects.latest("id")
        # cart = Cart(personid=lastestPerson, is_used="false")
        # cart.save()

        return redirect("login")


def addToCart(request, id):
    if request.method == "POST":
        quantity = request.POST.get("quantity")
        print(id, quantity)

        if len(quantity) <= 0 or (len(quantity) > 0 and int(quantity) == 0):
            messages.info(request, "Bạn chưa nhập số lượng")
            return redirect("homepage")

        personId = request.session.get("personId")
        person = Person.objects.get(id=personId)
        product = Product.objects.get(id=id)

        cart = Cart.objects.filter(productid=id).filter(personid=personId).filter(status=0)

        # cartItem = Cartitem.objects.filter(cart_id=cart.id).filter(productid=id)
        totalQuantity = int(quantity)

        # alreadyInCart = cartItem.exists()
        alreadyInCart = cart.exists()
        # nếu hàng đó đã trong giỏ hàng
        if alreadyInCart:
            totalQuantity += cart[0].quantity

        if product.quantity < totalQuantity:
            messages.info(request, "Không đủ hàng")
            return redirect("homepage")

        if alreadyInCart:
            # crrCartItem = Cartitem.objects.get(id=cartItem[0].id)
            # crrCartItem.quantity = totalQuantity
            # crrCartItem.save()
            update = Cart.objects.filter(personid=personId).filter(productid=id)[0]
            # data = {update}
            # return HttpResponse(update.quantity)
            update.quantity = totalQuantity
            update.status = 0
            update.save()
        else:
            # return HttpResponse(totalQuantity)
            # newCartItem = Cartitem(cart_id=cart, product_id=product, quantity=totalQuantity)
            # newCartItem.save()
            Cart.objects.create(personid=person, productid=product, quantity=totalQuantity,status=0)
            # new = Cart(personid=person, productid=product, quantity=totalQuantity)
            
            # new = Cart()
            # # new.personid = id
            # new.quantity = totalQuantity
            # new.save
            # new.save()
            return redirect("homepage")

        return redirect("homepage")


def getCart(request):
    if request.method == "GET":
        personId = request.session.get("personId")
        userId = request.session.get("userId")
        # return HttpResponse( request.session.get("userId"))
        # cart = Cart.objects.filter(personid=personId).filter(is_used="false")
        carts = Cart.objects.filter(personid=personId).filter(status=0)
        # cartItem = Cartitem.objects.filter(cart_id=cart)

        # calculate total
        total = 0
        # for item in cartItem:
        for item in carts:
            product = item.productid
            price = product.price
            sale = product.sale
            salePrice = price * (100 - sale) / 100
            total += salePrice * item.quantity
            item.productid.salePrice = round(salePrice)

        person = Person.objects.get(id=personId)
        address = Address.objects.get(userid=userId)
        # return HttpResponse(total)

        person.address = address.address
        name = Name.objects.get(userid= userId)
        person.name = name.first_name + " "+ name.last_name
        res = {"carts": carts, "person": person, "total": round(total)}
        return render(request, "customer/cart.html", res)


def removeFromCart(request):
    if request.method == "GET":
        cartId = request.GET.get("id")

        Cart.objects.filter(id=cartId).delete()
        return redirect("cart")

def applyOrder(request):
    personId = request.session.get("personId")
    userId = request.session.get("userId")
    # return HttpResponse( request.session.get("userId"))
    # cart = Cart.objects.filter(personid=personId).filter(is_used="false")
    carts = Cart.objects.filter(personid=personId).filter(status=0)
    # cartItem = Cartitem.objects.filter(cart_id=cart)

    # calculate total
    total = 0
    # for item in cartItem:
    for item in carts:
        product = item.productid
        price = product.price
        sale = product.sale
        salePrice = price * (100 - sale) / 100
        total += salePrice * item.quantity
        item.productid.salePrice = round(salePrice)

    person = Person.objects.get(id=personId)
    listAddress = Deliveryaddress.objects.filter(personid=personId)
    # return HttpResponse(carts)
    # province = Province.objects.get(personId=personId)
    name = Name.objects.get(userid= userId)
    person.name = name.first_name + " "+ name.last_name
    res = {"carts": carts, "person": person, "total": round(total),"listAddress":listAddress}
    return render(request, "customer/show_apply.html", res)

def listOrder(request):
    if request.GET.get("personId"):
        personId = request.GET.get("personId")
    else:
        personId = request.session.get("personId")
    listOrder = Order.objects.filter(personid=personId)
    for order in listOrder:
        transportation = Transportation.objects.get(orderid=order.id)
        address = Deliveryaddress.objects.get(id=transportation.deliveryaddressid)
        order.address = address.address
    res = {"listOrder": listOrder}
    return render(request, "customer/order.html", res)
def createOrder(request):
    
    
    deliveryId = request.GET.get('address')
    personId = request.session.get("personId")
    person = Person.objects.get(id=personId)
    carts = Cart.objects.filter(personid=personId).filter(status=0)
    delivery = Deliveryaddress.objects.all().get(id=deliveryId)
    method = request.GET.get("type_payment")
    if not carts.exists():
        return redirect("/cart")

    # cal total
    total = 0
    for item in carts:
        product = item.productid
        price = product.price
        sale = product.sale
        salePrice = price * (100 - sale) / 100
        total += salePrice * item.quantity

    created_time = datetime.now()
    # return HttpResponse(delivery)

    for cart in carts:
        order = Order(
            personid=person, cartid=cart, created_time=created_time, status=0, method=int(method), total_money=round(total)
        )
        order.save()
        Invoice.objects.create(orderid=order,created_time=created_time,money=total,status=0)
        Transportation.objects.create(orderid=order,deliveryaddressid=deliveryId,transport_fee=300000,status=0)
        cart.status = 1
        cart.save()

    # return HttpResponse(order)
    # return render(request, "customer/order.html", res)
    return redirect("/order/list")


def showOnlineSuccess(request):
    if request.method == "GET":
        bank = Bank.objects.all()
        res = {"bank": bank[0], "total": round(1000)}
        return render(request, "customer/created-order-online.html", res)


def showOfflineSuccess(request):
    if request.method == "GET":
        return render(request, "customer/created-order-offline.html")
