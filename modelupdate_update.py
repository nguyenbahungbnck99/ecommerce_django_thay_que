# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Address(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='UserId')  # Field name made lowercase.
    provinceid = models.ForeignKey('Province', models.DO_NOTHING, db_column='ProvinceId')  # Field name made lowercase.
    districtid = models.ForeignKey('District', models.DO_NOTHING, db_column='DistrictId')  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'address'


class Bank(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    invoiceid = models.ForeignKey('Invoice', models.DO_NOTHING, db_column='InvoiceId')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    account = models.CharField(db_column='Account', max_length=255, blank=True, null=True)  # Field name made lowercase.
    number = models.CharField(db_column='Number', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'bank'


class Book(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    supplierid = models.ForeignKey('Supplier', models.DO_NOTHING, db_column='SupplierId')  # Field name made lowercase.
    categoryid = models.ForeignKey('Category', models.DO_NOTHING, db_column='categoryId')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    author = models.CharField(db_column='Author', max_length=255, blank=True, null=True)  # Field name made lowercase.
    publisher = models.CharField(db_column='Publisher', max_length=255, blank=True, null=True)  # Field name made lowercase.
    category = models.CharField(db_column='Category', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'book'


class Cart(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    personid = models.ForeignKey('Person', models.DO_NOTHING, db_column='PersonId')  # Field name made lowercase.
    productid = models.ForeignKey('Product', models.DO_NOTHING, db_column='ProductId')  # Field name made lowercase.
    quantity = models.IntegerField(db_column='Quantity')  # Field name made lowercase.

    class Meta:
        db_table = 'cart'


class Category(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'category'


class Clothes(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    fashioncategoryid = models.ForeignKey('Fashioncategory', models.DO_NOTHING, db_column='FashionCategoryId')  # Field name made lowercase.
    supplierid = models.ForeignKey('Supplier', models.DO_NOTHING, db_column='SupplierId')  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    size = models.CharField(db_column='Size', max_length=255, blank=True, null=True)  # Field name made lowercase.
    branch = models.CharField(db_column='Branch', max_length=255, blank=True, null=True)  # Field name made lowercase.
    color = models.CharField(db_column='Color', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'clothes'


class Delivery(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    transportationid = models.ForeignKey('Transportation', models.DO_NOTHING, db_column='TransportationId')  # Field name made lowercase.
    shipperid = models.ForeignKey('Shipper', models.DO_NOTHING, db_column='ShipperId')  # Field name made lowercase.

    class Meta:
        db_table = 'delivery'


class Deliveryaddress(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    personid = models.ForeignKey('Person', models.DO_NOTHING, db_column='PersonId')  # Field name made lowercase.
    provinceid = models.ForeignKey('Province', models.DO_NOTHING, db_column='ProvinceId')  # Field name made lowercase.
    districtid = models.ForeignKey('District', models.DO_NOTHING, db_column='DistrictId')  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'deliveryaddress'


class Discount(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=255, blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(db_column='Description', max_length=255, blank=True, null=True)  # Field name made lowercase.
    start_date = models.DateField(db_column='Start_date', blank=True, null=True)  # Field name made lowercase.
    finish_date = models.DateField(db_column='Finish_date', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'discount'


class Discountcode(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    code = models.CharField(db_column='Code', max_length=255, blank=True, null=True)  # Field name made lowercase.
    start_date = models.DateField(db_column='Start_date', blank=True, null=True)  # Field name made lowercase.
    finish_date = models.DateField(db_column='Finish_date', blank=True, null=True)  # Field name made lowercase.
    total_discount = models.IntegerField(db_column='Total_discount')  # Field name made lowercase.
    total = models.IntegerField(db_column='Total')  # Field name made lowercase.

    class Meta:
        db_table = 'discountcode'


class Discountproduct(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    productid = models.ForeignKey('Product', models.DO_NOTHING, db_column='ProductId')  # Field name made lowercase.
    discountid = models.ForeignKey(Discount, models.DO_NOTHING, db_column='DiscountId')  # Field name made lowercase.

    class Meta:
        db_table = 'discountproduct'


class District(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    code = models.CharField(db_column='Code', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'district'


class Electro(models.Model):
    supplierid = models.ForeignKey('Supplier', models.DO_NOTHING, db_column='SupplierId')  # Field name made lowercase.
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    width = models.CharField(db_column='Width', max_length=255, blank=True, null=True)  # Field name made lowercase.
    height = models.CharField(db_column='Height', max_length=255, blank=True, null=True)  # Field name made lowercase.
    weight = models.CharField(db_column='Weight', max_length=255, blank=True, null=True)  # Field name made lowercase.
    branch = models.CharField(db_column='Branch', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'electro'


class Fashioncategory(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'fashioncategory'


class Inventory(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    clothesid = models.ForeignKey(Clothes, models.DO_NOTHING, db_column='ClothesId', blank=True, null=True)  # Field name made lowercase.
    electroid = models.ForeignKey(Electro, models.DO_NOTHING, db_column='ElectroId', blank=True, null=True)  # Field name made lowercase.
    bookid = models.ForeignKey(Book, models.DO_NOTHING, db_column='BookId', blank=True, null=True)  # Field name made lowercase.
    type = models.IntegerField(db_column='Type')  # Field name made lowercase.
    price = models.IntegerField(db_column='Price')  # Field name made lowercase.
    quantity = models.IntegerField(db_column='Quantity')  # Field name made lowercase.
    created_time = models.CharField(db_column='Created_time', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'inventory'


class Invoice(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    orderid = models.ForeignKey('Order', models.DO_NOTHING, db_column='OrderId')  # Field name made lowercase.
    created_time = models.CharField(db_column='Created_time', max_length=255, blank=True, null=True)  # Field name made lowercase.
    money = models.IntegerField(db_column='Money')  # Field name made lowercase.
    status = models.IntegerField(db_column='Status')  # Field name made lowercase.
    bank_id = models.IntegerField(db_column='Bank_id')  # Field name made lowercase.

    class Meta:
        db_table = 'invoice'


class Name(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='UserId')  # Field name made lowercase.
    first_name = models.CharField(db_column='First_name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    last_name = models.CharField(db_column='Last_name', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'name'


class New(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=255, blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(db_column='Description', max_length=255, blank=True, null=True)  # Field name made lowercase.
    content = models.CharField(db_column='Content', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'new'


class Order(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    cartid = models.ForeignKey(Cart, models.DO_NOTHING, db_column='CartId')  # Field name made lowercase.
    personid = models.ForeignKey('Person', models.DO_NOTHING, db_column='PersonId')  # Field name made lowercase.
    created_time = models.CharField(db_column='Created_time', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='Status')  # Field name made lowercase.
    method = models.IntegerField(db_column='Method')  # Field name made lowercase.
    discount_code = models.CharField(db_column='Discount_code', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'order'


class Payment(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    orderid = models.ForeignKey(Order, models.DO_NOTHING, db_column='OrderId')  # Field name made lowercase.
    shipperid = models.ForeignKey('Shipper', models.DO_NOTHING, db_column='ShipperId')  # Field name made lowercase.
    total = models.IntegerField(db_column='Total')  # Field name made lowercase.

    class Meta:
        db_table = 'payment'


class Person(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='UserId')  # Field name made lowercase.
    user_id = models.IntegerField(db_column='User_id')  # Field name made lowercase.
    gender = models.IntegerField(db_column='Gender')  # Field name made lowercase.
    birthday = models.CharField(db_column='Birthday', max_length=255, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(db_column='Phone', max_length=255, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'person'


class Product(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    inventoryid = models.ForeignKey(Inventory, models.DO_NOTHING, db_column='InventoryId')  # Field name made lowercase.
    type = models.IntegerField(db_column='Type')  # Field name made lowercase.
    sale = models.IntegerField(db_column='Sale')  # Field name made lowercase.
    price = models.IntegerField(db_column='Price')  # Field name made lowercase.
    quantity = models.IntegerField(db_column='Quantity')  # Field name made lowercase.
    created_time = models.CharField(db_column='Created_time', max_length=255, blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(db_column='Description', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'product'


class Province(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    code = models.CharField(db_column='Code', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'province'


class Review(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    productid = models.ForeignKey(Product, models.DO_NOTHING, db_column='ProductId')  # Field name made lowercase.
    rate = models.IntegerField(db_column='Rate')  # Field name made lowercase.
    content = models.CharField(db_column='Content', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'review'


class Shipper(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='UserId')  # Field name made lowercase.
    phone = models.CharField(db_column='Phone', max_length=255, blank=True, null=True)  # Field name made lowercase.
    attribute = models.CharField(db_column='Attribute', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gender = models.IntegerField(db_column='Gender')  # Field name made lowercase.

    class Meta:
        db_table = 'shipper'


class Staff(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='UserId')  # Field name made lowercase.
    gender = models.IntegerField(db_column='Gender')  # Field name made lowercase.
    brithday = models.CharField(db_column='Brithday', max_length=255, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(db_column='Phone', max_length=255, blank=True, null=True)  # Field name made lowercase.
    salary = models.IntegerField(db_column='Salary')  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'staff'


class Supplier(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    name = models.IntegerField(db_column='Name')  # Field name made lowercase.
    address = models.IntegerField(db_column='Address')  # Field name made lowercase.

    class Meta:
        db_table = 'supplier'


class Transportation(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    orderid = models.ForeignKey(Order, models.DO_NOTHING, db_column='OrderId')  # Field name made lowercase.
    deliveryaddressid = models.ForeignKey(Deliveryaddress, models.DO_NOTHING, db_column='DeliveryAddressId')  # Field name made lowercase.
    transport_fee = models.IntegerField(db_column='Transport_fee')  # Field name made lowercase.

    class Meta:
        db_table = 'transportation'


class User(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    username = models.CharField(db_column='Username', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pass_field = models.CharField(db_column='Pass', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    role = models.CharField(db_column='Role', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'user'
